package com.enigma.camshop.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "brand")
public class Brand {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(length = 100000)
    private String image;

    @OneToMany(mappedBy = "brand", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Product> listProduct;

    public Brand() {
    }

    public Brand(Long id, String name, String image, List<Product> listProduct) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.listProduct = listProduct;
    }

    public Brand(String name, String image, List<Product> listProduct) {
        this.name = name;
        this.image = image;
        this.listProduct = listProduct;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

