package com.enigma.camshop.entities;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@ToString
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(length = 100000)
    private String image;

    @OneToMany(mappedBy = "category", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Product> listProduct;

    public Category() {
    }

    public Category(Long id, String name, String image, List<Product> listProduct) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.listProduct = listProduct;
    }

    public Category(String name, String image, List<Product> listProduct) {
        this.name = name;
        this.image = image;
        this.listProduct = listProduct;
    }

    public Category(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
