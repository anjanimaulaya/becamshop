package com.enigma.camshop.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Setter
@Getter
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column
    private Double price;

    //    @Lob @Basic(fetch = FetchType.LAZY)
    @Column(length = 100000)
    private String image;

    @Column(columnDefinition = "TEXT")
    private String detail;

    @Column(name = "created")
    private Date created;

    @Column(name = "updated")
    private Date updated;

    @ManyToOne
    @JoinColumn(name = "id_category", nullable = false)
    private Category category;

    @ManyToOne
    @JoinColumn(name = "id_brand", nullable = false)
    private Brand brand;

    public Product() {
    }

    public Product(String name, Double price, String image, String detail, Date created, Date updated, Category category, Brand brand) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.detail = detail;
        this.created = created;
        this.updated = updated;
        this.category = category;
        this.brand = brand;
    }

    public Product(Long id, String name, Double price, String image, String detail, Date created, Date updated, Category category, Brand brand) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
        this.detail = detail;
        this.created = created;
        this.updated = updated;
        this.category = category;
        this.brand = brand;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Brand getBrand() {
		return brand;
	}

	public void setBrand(Brand brand) {
		this.brand = brand;
	}
    
}
