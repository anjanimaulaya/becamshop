package com.enigma.camshop.controllers;

import com.enigma.camshop.responses.AcccountCreateDTO;
import com.enigma.camshop.responses.AccountDTO;
import com.enigma.camshop.services.AccountService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("accounts")
public class AccountController {

    @Autowired
    private AccountService services;

    @ApiOperation(value = "Get all account")
    @GetMapping("")
    public ResponseEntity<List<AccountDTO>> getAll() {
        return ResponseEntity.ok().body(services.findAll());
    }

    @GetMapping("search")
    @ApiOperation(value = "Get account by name")
    public AccountDTO getByName(@Valid @RequestParam String name) {
        return services.getByName(name);
    }

    @ApiOperation(value = "Create account")
    @PostMapping("")
    public AccountDTO createAccount(@Valid @RequestBody AcccountCreateDTO request) {
        return services.save(request);
    }

    @GetMapping("/id/{id}")
    @ApiOperation(value = "Get account by id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Id success found"),
    })
    public ResponseEntity<AccountDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(services.findById(id));
    }

    @ApiOperation(value = "Update account")
    @PutMapping("")
    public AccountDTO updateAccount(@RequestBody AccountDTO request) {
        return services.updateAccount(request);
    }

    @ApiOperation(value = "Delete account by id")
    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable Long id) {
        services.deleteById(id);
    }


}
