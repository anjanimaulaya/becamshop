package com.enigma.camshop.controllers;

import com.enigma.camshop.entities.Product;
import com.enigma.camshop.responses.*;
import com.enigma.camshop.services.ProductService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("products")
public class ProductController {

    @Autowired
    private ProductService services;

    @ApiOperation(value = "Get all product")
    @GetMapping("")
    public ResponseEntity<List<ProductDTO>> findAll() {
        return ResponseEntity.ok().body(services.findAll());
    }

    @ApiOperation(value = "Get product by name")
    @GetMapping("/search")
    public ResponseEntity<List<ProductDTO>> findProductByName(@RequestParam String name) {
        return ResponseEntity.ok().body(services.findProductByName(name));
    }

    @ApiOperation(value = "Create product")
    @PostMapping("")
    public String createProduct(@RequestBody ProductCreateDTO request) {
        return services.createProduct(request);
    }

    @ApiOperation(value = "Update product")
    @PutMapping("")
    public String updateProduct(@RequestBody ProductUpdateDTO request) {
        return services.updateProduct(request);
    }

    @GetMapping("/id/{id}")
    @ApiOperation(value = "Get product by id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Id success found"),
    })
    public ResponseEntity<ProductDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(services.findById(id));
    }

    @ApiOperation(value = "Delete product by id")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        services.deleteProduct(id);
    }

    @ApiOperation(value = "Find product by brand name")
    @GetMapping("byBrand")
    public List<ProductBrandDTO> getProductByBrandName(@Valid @RequestParam String name) {
        return services.getListProductByBrandName(name);
    }

    @ApiOperation(value = "Find product by category name")
    @GetMapping("byCategory")
    public List<ProductCategoryDTO> getProductByCategoryName(@Valid @RequestParam String name) {
        return services.getListProductByCategoryName(name);
    }

    @ApiOperation(value = "Filter product by price")
    @GetMapping("filterByPrice")
    public List<ProductPriceDTO> getProductByPrice(@Valid @RequestParam Double min, Double max) {
        return services.getListProductByPrice(min, max);
    }

    @ApiOperation(value = "Get product by ascending price")
    @GetMapping("ascPrice")
    public List<ProductPriceDTO> getProductByAscendingPrice() {
        return services.getListProductByAscendingPrice();
    }

    @ApiOperation(value = "Get product by descending price")
    @GetMapping("descPrice")
    public List<ProductPriceDTO> getProductByDescendingPrice() {
        return services.getListProductByDescendingPrice();
    }

    @ApiOperation(value = "Get Products With Paging")
    @GetMapping("/paging/{pageNo}")
    public ResponseEntity<List<Product>> getProductsWithPaging(@PathVariable Integer pageNo) {
        return ResponseEntity.ok().body(services.getProductsWithPaging(pageNo));
    }

    @ApiOperation(value = "Filter product by brand and category")
    @GetMapping("filterByBrandAndCategory")
    public List<Product> getProductByBrandAndCategory(@Valid @RequestParam String brandName, String categoryName) {
        return services.getListProductByBrandNameAndCategory(brandName, categoryName);
    }
}
