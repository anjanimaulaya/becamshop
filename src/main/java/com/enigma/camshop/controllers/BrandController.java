package com.enigma.camshop.controllers;

import com.enigma.camshop.responses.BrandDTO;
import com.enigma.camshop.responses.BrandRequest;
import com.enigma.camshop.responses.ProductDTO;
import com.enigma.camshop.services.BrandService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("brands")
public class BrandController {

    @Autowired
    private BrandService services;

    @ApiOperation(value = "Get all brand")
    @GetMapping("")
    public ResponseEntity<List<BrandDTO>> getAll() {
        return ResponseEntity.ok().body(services.findAll());
    }

    @GetMapping("/id/{id}")
    @ApiOperation(value = "Get brand by id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Id success found"),
    })
    public ResponseEntity<BrandDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(services.findById(id));
    }

    @GetMapping("search")
    @ApiOperation(value = "Get brand by name")
    public BrandDTO getByName(@Valid @RequestParam String name) {
        return services.getByName(name);
    }

    @ApiOperation(value = "Create brand")
    @PostMapping("")
    public BrandDTO createBrand(@Valid @RequestBody BrandRequest request) {
        return services.save(request);
    }

    @ApiOperation(value = "Delete brand by id")
    @DeleteMapping("/{id}")
    public void delete(@Valid @PathVariable Long id) {
        services.deleteById(id);
    }
}
