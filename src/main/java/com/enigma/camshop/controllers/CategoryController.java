package com.enigma.camshop.controllers;

import com.enigma.camshop.responses.CategoryDTO;
import com.enigma.camshop.responses.ProductDTO;
import com.enigma.camshop.responses.RequestCategoryDTO;
import com.enigma.camshop.services.CategoryService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin("*")
@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private CategoryService services;

    @GetMapping("")
    public ResponseEntity<List<CategoryDTO>> findAll() {
        return ResponseEntity.ok().body(services.findAll());
    }

    @GetMapping("/id/{id}")
    @ApiOperation(value = "Get category by id")
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Id success found"),
    })
    public ResponseEntity<CategoryDTO> findById(@PathVariable Long id) {
        return ResponseEntity.ok().body(services.findById(id));
    }

    @PostMapping("")
    public RequestCategoryDTO createCategory(@RequestBody RequestCategoryDTO request) {
        return services.createCategory(request);
    }

    @GetMapping("search")
    @ApiOperation(value = "Get brand by name")
    public CategoryDTO getByName(@Valid @RequestParam String name) {
        return services.getByName(name);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        services.deleteCategory(id);
    }
}
