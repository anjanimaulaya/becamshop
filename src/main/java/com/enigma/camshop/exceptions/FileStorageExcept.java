package com.enigma.camshop.exceptions;

import java.io.IOException;

public class FileStorageExcept extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String msg;

    public FileStorageExcept(String msg, IOException ex) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
