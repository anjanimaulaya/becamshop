package com.enigma.camshop.services;

import com.enigma.camshop.entities.Brand;
import com.enigma.camshop.exceptions.NotFoundException;
import com.enigma.camshop.repositories.BrandRepository;
import com.enigma.camshop.responses.BrandDTO;
import com.enigma.camshop.responses.BrandRequest;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class BrandService {
    @Autowired
    private BrandRepository repo;
    
    private static final Logger log = LoggerFactory.getLogger(BrandService.class);

    public Brand convertBrandEntity(BrandDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Brand brand = modelMapper.map(dto, Brand.class);
        return brand;
    }

    public BrandDTO convertBrandDto(Brand brand) {
        ModelMapper modelMapper = new ModelMapper();
        BrandDTO dto = modelMapper.map(brand, BrandDTO.class);
        return dto;
    }

    public BrandDTO convertOptionalBrand(Optional<Brand> brand) {
        ModelMapper modelMapper = new ModelMapper();
        BrandDTO dto = modelMapper.map(brand, BrandDTO.class);
        return dto;
    }

    public List<BrandDTO> findAll() {
        log.info("get all brand");
        List<BrandDTO> list = new ArrayList<>();
        repo.findAll().forEach(brand -> {
            list.add(new BrandDTO(brand.getId(), brand.getName(), brand.getImage()));
        });
        return list;
    }

    public BrandDTO getByName(String name) {
        log.info("get brand by name");
        Brand brand = repo.findBrandByName(name);
        return convertBrandDto(brand);
    }

    public BrandDTO findById(Long id) {
        log.info("Show brand by Id");

        Brand brand = repo.findById(id).orElseThrow(() -> new NotFoundException(id + " is not found"));
        ModelMapper modelMapper = new ModelMapper();
        BrandDTO dto = modelMapper.map(brand, BrandDTO.class);

        return dto;
    }

    public BrandDTO save(BrandRequest request) {
        log.info("post brand");
        Brand checkBrand = repo.findBrandByName(request.getName());
        if (checkBrand != null) {
            throw new NotFoundException(request.getName() + " brand already saved");
        }
        ModelMapper modelMapper = new ModelMapper();
        Brand brand = modelMapper.map(request, Brand.class);
        repo.save(brand);
        return convertBrandDto(brand);
    }

    public void deleteByName(String name) {
        log.info("delete brand");
        repo.deleteByName(name);
    }

    public void deleteById(Long id) {
        log.info("delete by id");
        repo.deleteById(id);
    }


}
