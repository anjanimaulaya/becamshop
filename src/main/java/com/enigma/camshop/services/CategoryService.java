package com.enigma.camshop.services;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enigma.camshop.entities.Category;
import com.enigma.camshop.exceptions.NotFoundException;
import com.enigma.camshop.repositories.CategoryRepository;
import com.enigma.camshop.responses.CategoryDTO;
import com.enigma.camshop.responses.RequestCategoryDTO;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CategoryService {

    @Autowired
    private CategoryRepository repo;
    
    private static final Logger log = LoggerFactory.getLogger(CategoryService.class);

    public Category convertCategoryEntity(CategoryDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Category product = modelMapper.map(dto, Category.class);
        return product;
    }

    public CategoryDTO convertCategoryDto(Category category) {
        ModelMapper modelMapper = new ModelMapper();
        CategoryDTO dto = modelMapper.map(category, CategoryDTO.class);
        return dto;
    }

    public CategoryDTO convertOptionalCategory(Optional<Category> category) {
        ModelMapper modelMapper = new ModelMapper();
        CategoryDTO dto = modelMapper.map(category, CategoryDTO.class);
        return dto;
    }

    public List<CategoryDTO> findAll() {
        List<CategoryDTO> list = new ArrayList<>();
        repo.findAll().forEach(category -> {
            list.add(new CategoryDTO(category.getId(), category.getName(), category.getImage()));
        });
        log.info("get all product");
        return list;
    }

    //create
    public RequestCategoryDTO createCategory(RequestCategoryDTO request) {
        Category category = new Category(request.getName(), request.getImage());
        repo.save(category);
        ModelMapper mod = new ModelMapper();
        RequestCategoryDTO reqdto = mod.map(category, RequestCategoryDTO.class);
        log.info("create acc");
        return reqdto;
    }

    public CategoryDTO findById(Long id) {
        log.info("Show category by Id");

        Category category = repo.findById(id).orElseThrow(() -> new NotFoundException(id + " is not found"));
        ModelMapper modelMapper = new ModelMapper();
        CategoryDTO dto = modelMapper.map(category, CategoryDTO.class);

        return dto;
    }

    public CategoryDTO getByName(String name) {
        log.info("get category by name");
        Category category = repo.findCategoryByName(name);
        return convertCategoryDto(category);
    }

    //delete
    public void deleteCategory(Long id) {
        log.info("Delete");
        repo.deleteById(id);
    }
}
