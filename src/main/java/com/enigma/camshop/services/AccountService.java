package com.enigma.camshop.services;

import com.enigma.camshop.entities.Account;
import com.enigma.camshop.exceptions.NotFoundException;
import com.enigma.camshop.repositories.AccountRepository;
import com.enigma.camshop.responses.AcccountCreateDTO;
import com.enigma.camshop.responses.AccountDTO;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Slf4j
@Service
public class AccountService {

    @Autowired
    private AccountRepository repo;
    
    private static final Logger log = LoggerFactory.getLogger(AccountService.class);

    public Account convertAccountEntity(AccountDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Account account = modelMapper.map(dto, Account.class);
        return account;
    }

    public AccountDTO convertAccountDto(Account account) {
        ModelMapper modelMapper = new ModelMapper();
        AccountDTO dto = modelMapper.map(account, AccountDTO.class);
        return dto;
    }

    public AccountDTO convertOptionalAccount(Optional<Account> account) {
        ModelMapper modelMapper = new ModelMapper();
        AccountDTO dto = modelMapper.map(account, AccountDTO.class);
        return dto;
    }

    public List<AccountDTO> findAll() {
        log.info("get all account");
        List<AccountDTO> list = new ArrayList<>();
        repo.findAll().forEach(account -> {
            list.add(new AccountDTO(account.getId(), account.getName(), account.getPassword(), account.getEmail(), account.getGender(), account.getAddress(), account.getRole()));
        });
        return list;
    }

    public AccountDTO getByName(String name) {
        log.info("get account by name");
        Account account = repo.findAccountByName(name);
        return convertAccountDto(account);
    }

    public AccountDTO findById(Long id) {
        log.info("Show category by Id");

        Account account = repo.findById(id).orElseThrow(() -> new NotFoundException(id + " is not found"));
        ModelMapper modelMapper = new ModelMapper();
        AccountDTO dto = modelMapper.map(account, AccountDTO.class);

        return dto;
    }

    public AccountDTO save(AcccountCreateDTO request) {
        log.info("post account");
        Account checkAccount = repo.findAccountByName(request.getName());
        if (checkAccount != null) {
            throw new NotFoundException(request.getName() + " account already saved");
        }
        ModelMapper modelMapper = new ModelMapper();
        Account account = modelMapper.map(request, Account.class);
        repo.save(account);
        return convertAccountDto(account);
    }

    public AccountDTO updateAccount(AccountDTO request) {
        log.info("Update account");
        Optional<Account> account = repo.findById(request.getId());

        account.get().setName(request.getName());
        account.get().setPassword(request.getPassword());
        account.get().setEmail(request.getEmail());
        account.get().setGender(request.getGender());
        account.get().setAddress(request.getAddress());
        account.get().setRole(request.getRole());
        repo.save(account.get());
        return request;

    }

    public void deleteByName(String name) {
        log.info("delete account");
        repo.deleteByName(name);
    }

    public void deleteById(Long id) {
        log.info("delete by id");
        repo.deleteById(id);
    }
}
