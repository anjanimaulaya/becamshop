package com.enigma.camshop.services;

import com.enigma.camshop.entities.Product;
import com.enigma.camshop.exceptions.FileStorageExcept;
import com.enigma.camshop.exceptions.FileStorageException;
import com.enigma.camshop.exceptions.MyFileNotFoundException;
import com.enigma.camshop.property.FileStorageProperties;
import com.enigma.camshop.repositories.ProductRepository;
import com.enigma.camshop.responses.UploadFileResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Optional;

@Service
public class FileStorageService {
    private final Path fileStorageLocation;

    @Autowired
    ProductRepository repo;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties) throws FileStorageExcept {
        this.fileStorageLocation = Paths.get(((FileStorageProperties) fileStorageProperties).getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageExcept("Could not create the directory where the uploaded files will be stored.", (IOException) ex);
        }
    }

    public String storeFile(MultipartFile file) throws FileStorageExcept, FileStorageException {
        // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if(fileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            return fileName;
        } catch (IOException | FileStorageException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!");
        }
    }

    public Resource loadFileAsResource(String fileName) throws MyFileNotFoundException {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("File not found " + fileName, ex);
        }
    }

    public UploadFileResponse saveImage(UploadFileResponse file, Long id){
        Optional<Product> product=repo.findById(id);
        //localhost yang di pake sekarang
        String fileLink = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/images/")
                .path(file.getFileName())
                .toUriString();
        product.get().setImage(fileLink);
        repo.save(product.get());
        return new UploadFileResponse(file.getFileName(),file.getFileDownloadUri(),file.getFileType(),file.getSize());
    }

}
