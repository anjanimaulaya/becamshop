package com.enigma.camshop.services;

import com.enigma.camshop.entities.Brand;
import com.enigma.camshop.entities.Category;
import com.enigma.camshop.entities.Product;
import com.enigma.camshop.exceptions.NotFoundException;
import com.enigma.camshop.repositories.BrandRepository;
import com.enigma.camshop.repositories.CategoryRepository;
import com.enigma.camshop.repositories.ProductRepository;
import com.enigma.camshop.responses.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class ProductService {
    @Autowired
    private ProductRepository repo;
    @Autowired
    private CategoryRepository repoC;
    @Autowired
    private BrandRepository repoB;
    @Autowired
    private CategoryService serviceC;
    @Autowired
    private BrandService serviceB;

    private static final Logger log = LoggerFactory.getLogger(ProductService.class);
    
    public Product convertProductEntity(ProductDTO dto) {
        ModelMapper modelMapper = new ModelMapper();
        Product product = modelMapper.map(dto, Product.class);
        return product;
    }

    public ProductDTO convertProductDto(Product product) {
        ModelMapper modelMapper = new ModelMapper();
        ProductDTO dto = modelMapper.map(product, ProductDTO.class);
        return dto;
    }

    public String createProduct(ProductCreateDTO request) {
        Optional<Category> category = repoC.findById(request.getCategory());
        Optional<Brand> brand = repoB.findById(request.getBrand());
        repo.save(new Product(request.getName(), request.getPrice(), request.getImage(),
                request.getDetail(), request.getCreated(), request.getUpdated(), category.get(), brand.get()));
        log.info("create acc");
        return "SUCCESS SAVED!";
    }

    public List<ProductDTO> findAll() {
        List<ProductDTO> list = new ArrayList<>();
        repo.findAll().forEach(product -> {
            list.add(new ProductDTO(product.getId(), product.getName(),
                    product.getPrice(), product.getImage(), product.getDetail(), product.getCreated(),
                    product.getUpdated(), product.getBrand().getName(), product.getCategory().getName()));
        });
        log.info("get all product");
        return list;
    }

    public ProductDTO findById(Long id) {
        log.info("Show product by Id");

        Product account = repo.findById(id).orElseThrow(() -> new NotFoundException(id + " is not found"));
        ModelMapper modelMapper = new ModelMapper();
        ProductDTO dto = modelMapper.map(account, ProductDTO.class);

        return dto;
    }

    public List<ProductDTO> findProductByName(String name) {
        List<ProductDTO> list = new ArrayList<>();
        repo.findByNameContainsIgnoreCase(name).forEach(product -> {
            list.add(new ProductDTO(product.getId(), product.getName(),
                    product.getPrice(), product.getImage(), product.getDetail(), product.getCreated(),
                    product.getUpdated(), product.getBrand().getName(), product.getCategory().getName()));
        });
        log.info("Get by Name");
        return list;
    }

    //update
    public String updateProduct(ProductUpdateDTO request) {
        Optional<Category> category = repoC.findById(request.getCategory());
        Optional<Brand> brand = repoB.findById(request.getBrand());
        repo.save(new Product(request.getId(), request.getName(), request.getPrice(), request.getImage(),
                request.getDetail(), request.getCreated(), request.getUpdated(), category.get(), brand.get()));
        log.info("update acc");
        return "SUCCESS UPDATED!";
    }

    public void deleteProduct(Long id) {
        log.info("Delete");
        repo.deleteById(id);
    }

    public List<ProductBrandDTO> getListProductByBrandName(String name) {

        BrandDTO brandDto = serviceB.getByName(name);
        if (brandDto == null) {
            throw new NotFoundException(name + " brand is not found");
        }
        Brand brand = serviceB.convertBrandEntity(brandDto);
        List<Product> list = repo.findByBrand(brand);
        List<ProductBrandDTO> listDto = new ArrayList<ProductBrandDTO>();
        list.forEach(product -> {
            listDto.add(new ProductBrandDTO(product.getId(), product.getName(), product.getPrice(), product.getImage(), product.getDetail(), product.getBrand().getName(), product.getCategory().getName()));
        });
        return listDto;
    }

    public List<ProductCategoryDTO> getListProductByCategoryName(String name) {
        CategoryDTO categoryDto = serviceC.getByName(name);
        if (categoryDto == null) {
            throw new NotFoundException(name + " category is not found");
        }
        Category category = serviceC.convertCategoryEntity(categoryDto);
        List<Product> list = repo.findByCategory(category);
        List<ProductCategoryDTO> listDto = new ArrayList<ProductCategoryDTO>();
        list.forEach(product -> {
            listDto.add(new ProductCategoryDTO(product.getId(), product.getName(), product.getPrice(), product.getImage(), product.getDetail(), product.getBrand().getName(), product.getCategory().getName()));
        });
        return listDto;
    }

    public List<ProductPriceDTO> getListProductByPrice(Double min, Double max) {
        List<Product> list = repo.findAll();

        List<ProductPriceDTO> listDto = new ArrayList<ProductPriceDTO>();
        list.forEach(product -> {
            if (product.getPrice() >= min && product.getPrice() <= max) {
                listDto.add(new ProductPriceDTO(product.getId(), product.getName(), product.getPrice(), product.getImage(), product.getDetail(), product.getBrand().getName(), product.getCategory().getName()));
            }
        });

        return listDto;
    }

    public List<ProductPriceDTO> getListProductByAscendingPrice() {
        List<Product> list = repo.findByOrderByPriceAsc();
        List<ProductPriceDTO> listDto = new ArrayList<ProductPriceDTO>();
        list.forEach(product -> {
            listDto.add(new ProductPriceDTO(product.getId(), product.getName(), product.getPrice(), product.getImage(), product.getDetail(), product.getBrand().getName(), product.getCategory().getName()));
        });

        return listDto;
    }

    public List<ProductPriceDTO> getListProductByDescendingPrice() {
        List<Product> list = repo.findByOrderByPriceDesc();
        List<ProductPriceDTO> listDto = new ArrayList<ProductPriceDTO>();
        list.forEach(product -> {
            listDto.add(new ProductPriceDTO(product.getId(), product.getName(), product.getPrice(), product.getImage(), product.getDetail(), product.getBrand().getName(), product.getCategory().getName()));
        });

        return listDto;
    }

    public List<Product> getProductsWithPaging(Integer pageNo) {
        Integer pageSize = 8;
        Pageable paging = PageRequest.of(pageNo - 1, pageSize);
        Page<Product> pagedResult = repo.findAll(paging);
        return pagedResult.getContent();
    }

    public List<Product> getListProductByBrandNameAndCategory(String brandName, String categoryName) {
        BrandDTO dtoBrand = serviceB.getByName(brandName);
        CategoryDTO dtoCategory = serviceC.getByName(categoryName);
        if (dtoBrand == null || dtoCategory == null) {
            throw new NotFoundException(brandName + " brand with category " + categoryName + " is not found");
        }
        List<Product> list = new ArrayList<Product>();
        repo.findAll().forEach(product -> {
            if (product.getBrand().getName() == dtoBrand.getName() && product.getCategory().getName() == dtoCategory.getName()) {
                list.add(product);
            }
        });

        return list;
    }
}
