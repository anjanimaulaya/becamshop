package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

public class ProductBrandDTO {
    private Long id;
    private String name;
    private Double price;
    private String image;
    private String detail;
    private String brand;
    private String category;
	
    public ProductBrandDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductBrandDTO(Long id, String name, Double price, String image, String detail, String brand,
			String category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.detail = detail;
		this.brand = brand;
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
    
    
}
