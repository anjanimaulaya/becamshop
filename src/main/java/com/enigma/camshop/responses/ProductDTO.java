package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

@Setter
@Getter
public class ProductDTO {
    private Long id;
    private String name;
    private Double price;
    private String image;
    private String detail;
    private Date created;
    private Date updated;
    private String brand;
    private String category;

    public ProductDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductDTO(Long id, String name, Double price, String image, String detail, Date created, Date updated,
			String brand, String category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.detail = detail;
		this.created = created;
		this.updated = updated;
		this.brand = brand;
		this.category = category;
	}

	public ProductDTO(String name, Double price, String image, String detail, Date created, Date updated, String brand, String category) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.detail = detail;
        this.created = created;
        this.updated = updated;
        this.brand = brand;
        this.category = category;
    }

    public ProductDTO(String name, Double price, String image, String detail, String brand) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.detail = detail;
        this.brand = brand;
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
    
    
}
