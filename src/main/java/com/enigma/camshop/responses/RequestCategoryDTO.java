package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

public class RequestCategoryDTO {
    @NotEmpty(message = "Category name can't be empty")
    private String name;
    @NotEmpty(message = "Category image can't be empty")
    private String image;
	
    public RequestCategoryDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RequestCategoryDTO(@NotEmpty(message = "Category name can't be empty") String name,
			@NotEmpty(message = "Category image can't be empty") String image) {
		super();
		this.name = name;
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
    
    
}
