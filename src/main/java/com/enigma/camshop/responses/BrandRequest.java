package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;

public class BrandRequest {

    @NotEmpty(message = "Brand image can't be empty")
    private String image;
    @NotEmpty(message = "Brand name can't be empty")
    private String name;
	
    public BrandRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BrandRequest(@NotEmpty(message = "Brand image can't be empty") String image,
			@NotEmpty(message = "Brand name can't be empty") String name) {
		super();
		this.image = image;
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    
    


}
