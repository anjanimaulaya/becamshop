package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Date;

public class ProductUpdateDTO {
    private Long id;
    private String name;
    private Double price;
    private String image;
    private String detail;
    private Date created;
    private Date updated;
    private Long brand;
    private Long category;
	
    public ProductUpdateDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ProductUpdateDTO(Long id, String name, Double price, String image, String detail, Date created, Date updated,
			Long brand, Long category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.image = image;
		this.detail = detail;
		this.created = created;
		this.updated = updated;
		this.brand = brand;
		this.category = category;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public Long getBrand() {
		return brand;
	}

	public void setBrand(Long brand) {
		this.brand = brand;
	}

	public Long getCategory() {
		return category;
	}

	public void setCategory(Long category) {
		this.category = category;
	}
    
    
}
