package com.enigma.camshop.responses;

import javax.validation.constraints.NotEmpty;

public class AcccountCreateDTO {

    private String name;
    private String password;
    private String email;
    private String gender;
    private String address;
    private Integer role;
    
	public AcccountCreateDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AcccountCreateDTO(String name, String password, String email, String gender, String address, Integer role) {
		super();
		this.name = name;
		this.password = password;
		this.email = email;
		this.gender = gender;
		this.address = address;
		this.role = role;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}
	
    
}
