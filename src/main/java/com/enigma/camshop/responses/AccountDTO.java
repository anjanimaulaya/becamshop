package com.enigma.camshop.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;

public class AccountDTO {
    private Long id;
    private String name;
    private String password;
    private String email;
    private String gender;
    private String address;
    private Integer role;

	public AccountDTO(Long id, String name, String password, String email, String gender, String address,
			Integer role) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.email = email;
		this.gender = gender;
		this.address = address;
		this.role = role;
	}
	
	   public AccountDTO(String name, String password, String email, String gender, String address, Integer role) {
	        this.name = name;
	        this.password = password;
	        this.email = email;
	        this.gender = gender;
	        this.address = address;
	        this.role = role;
	    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}
	
	
}
