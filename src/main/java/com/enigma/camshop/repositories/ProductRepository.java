package com.enigma.camshop.repositories;

import com.enigma.camshop.entities.Brand;
import com.enigma.camshop.entities.Category;
import com.enigma.camshop.entities.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    List<Product> findByNameContainsIgnoreCase(String name);

    List<Product> findByBrand(Brand brand);

    List<Product> findByCategory(Category category);

    List<Product> findByOrderByPriceAsc();

    List<Product> findByOrderByPriceDesc();
}
