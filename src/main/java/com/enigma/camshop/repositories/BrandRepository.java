package com.enigma.camshop.repositories;

import com.enigma.camshop.entities.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Long> {
    Brand findBrandByName(String name);

    void deleteByName(String name);
}
