package com.enigma.camshop.repositories;

import com.enigma.camshop.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Account findAccountByName(String name);

    void deleteByName(String name);
}
